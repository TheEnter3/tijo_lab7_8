package pl.edu.pwsztar.domain.dto;

import java.io.File;

public class FileDto {

   private File file;

    public FileDto() {

    }

    public FileDto(Builder builder){
        file = builder.file;
    }

    public File getFile() {
        return file;
    }


    public static class Builder{
        private File file;

        public Builder(){

        }
        public Builder file(File file){
            this.file = file;
            return this;
        }

        public FileDto build(){
            return new FileDto(this);
        }
    }
}
