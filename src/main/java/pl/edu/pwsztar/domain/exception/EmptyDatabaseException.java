package pl.edu.pwsztar.domain.exception;

public class EmptyDatabaseException extends Exception{
    public EmptyDatabaseException() {
    }

    public EmptyDatabaseException(String message) {
        super(message);
    }
}
