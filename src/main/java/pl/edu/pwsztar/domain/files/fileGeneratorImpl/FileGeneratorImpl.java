package pl.edu.pwsztar.domain.files.fileGeneratorImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.FileDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.files.FileGenerator;
import pl.edu.pwsztar.service.MovieService;

import java.io.*;
import java.util.List;

@Component
public class FileGeneratorImpl implements FileGenerator {

    private final MovieService movieService;

    @Autowired
    public FileGeneratorImpl(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public InputStreamResource toTxt(FileDto fileDto) throws IOException {

        File file = WriteToFile(fileDto.getFile());

        InputStream stream = new FileInputStream(file);


        return new InputStreamResource(stream);
    }

    private File WriteToFile(File file) throws IOException{

        FileOutputStream fileOutputStream=new FileOutputStream(file);

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fileOutputStream));

        writeMovieDtoToFile(bufferedWriter);

        bufferedWriter.close();

        fileOutputStream.flush();
        fileOutputStream.close();

        return file;
    }

    private void writeMovieDtoToFile(BufferedWriter bufferedWriter) throws IOException{
        List<MovieDto> movieDtoList = movieService.getMoviesByYearDesc();
        for (MovieDto movieDto : movieDtoList) {
            bufferedWriter.write(movieDto.getYear() + " " + movieDto.getTitle());
            bufferedWriter.newLine();
        }
    }
}
